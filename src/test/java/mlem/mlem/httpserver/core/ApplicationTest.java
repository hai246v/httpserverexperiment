package mlem.mlem.httpserver.core;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class ApplicationTest {

    @Test
    public void testMakeResponseBytes() throws Exception {
        Response response = new Response("text/plain", "hello ,world".getBytes());
        System.out.println(new String(response.makeResponseBytes()));
        assert (new String(response.makeResponseBytes())).endsWith("hello ,world");
    }
}