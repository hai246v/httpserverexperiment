package mlem.mlem.httpserver;

import mlem.mlem.httpserver.core.Application;

public class Main {
    public static void main(final String[] args) throws Exception {
        final Application application;
        application = new Application(8080);
        application.run();
    }
}



