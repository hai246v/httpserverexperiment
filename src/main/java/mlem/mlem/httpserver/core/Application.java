package mlem.mlem.httpserver.core;

public class Application {
    private final int port;
    private final java.net.ServerSocket myServerSocket;

    public Application(int port) throws Exception {
        this.port = port;
        myServerSocket = new java.net.ServerSocket(port);
    }

    public void run() throws Exception {
        while (true) {
            java.net.Socket clientSocket = myServerSocket.accept();
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Handler handler = new Handler(clientSocket);
                        handler.handle();
                    } catch (Exception ignored) {
                    }
                }
            });
            thread.run();
        } // end while
    }
}
