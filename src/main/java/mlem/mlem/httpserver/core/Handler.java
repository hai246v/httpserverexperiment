package mlem.mlem.httpserver.core;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Handler {
    private InputStream input;
    private OutputStream output;
    private java.net.Socket clientSocket;

    Handler(java.net.Socket clientSocket) throws Exception {
        this.clientSocket = clientSocket;
        input = clientSocket.getInputStream();
        output = clientSocket.getOutputStream();
    }

    void handle() throws Exception {
        Request request = new Request(input);
        byte[] data;
        String contentType = "";
        Path path = Paths.get(System.getProperty("user.home"), request.getPath());
        try {
            data = Files.readAllBytes(path);
            contentType = getContentType(path);
        } catch (Exception e) {
            data = ("<h1 style='color:red;'>Can not read this file</h1>" + path.toString()).getBytes();
        }
        Response response = new Response(contentType, data);
        // Write response
        output.write(response.makeResponseBytes());
        // Close streams
        input.close();
        output.close();
    }

    private String getContentType(Path path) {
        if (path.getFileName().toString().split("\\.").length > 1) {
            int ext = path.getFileName().toString().split("\\.").length - 1; // EX: image.png should be 1
            switch (path.getFileName().toString().split("\\.")[ext]) {
                case "png":
                    return "image/png";
                case "jpg":
                    return "image/jpeg";
                case "gif":
                    return "image/gif";
            }
        }
        return "text/plain";
    }
}
