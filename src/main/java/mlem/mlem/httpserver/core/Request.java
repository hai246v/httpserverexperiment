package mlem.mlem.httpserver.core;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Request {
    private InputStream inputStream;

    Request (InputStream inputStream) {
        this.inputStream = inputStream;
    }

    String getPath() throws Exception {
        String path = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String s = "";
        while (!(s = reader.readLine()).isEmpty()) {
            if (s.split(" ")[0].equals("GET")) {
                return s.split(" ")[1];
            }
        }
        throw new Error("Invalid resquest");
    }
}
