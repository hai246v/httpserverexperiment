package mlem.mlem.httpserver.core;

import java.io.ByteArrayOutputStream;

class Response {
    private final byte[] data;
    private final String contentType;

    Response(String contentType, byte[] data) {
        this.contentType = contentType;
        this.data = data;
    }

    public byte[] makeResponseBytes() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            outputStream.write(makeHeader().getBytes());
            outputStream.write("\r\n".getBytes());
            outputStream.write(data);
            return outputStream.toByteArray();
        } catch (Exception e) {
            return ("HTTP/1.1 500 Internal Server Error\r\n" +
                    "Server: my stupid http server\r\n" +
                    "\r\n ").getBytes();
        }
    }

    private String makeHeader() {
        return "HTTP/1.1 200 OK\r\n" +
                "Server: my stupid http server\r\n" +
                "Content-Length: " + Integer.toString(data.length) + "\r\n" +
                "Content-Type: " + contentType + "\r\n" +
                "Connection: keep-alive\r\n";
    }
}
